import express from 'express';
import socket from 'socket.io';
import serverConfig from './src/config/server';

const app = express();

const server = app.listen(process.env.PORT || serverConfig.PORT, () => {
  console.log(`Server running on port ${process.env.PORT || serverConfig.PORT}`);
});

const io = socket(server);

const connectedUsers = [];

const checkNickname = nickname => connectedUsers.findIndex(e => e.nickname === nickname);

io.on('connection', (sock) => {
  sock.on('SET_NICKNAME', (data) => {
    if (checkNickname(data) === -1) {
      const user = {
        id: sock.id,
        nickname: data,
      };
      connectedUsers.push(user);
      sock.emit('SET_NICKNAME_SUCCESS');
    } else {
      sock.emit('SET_NICKNAME_FAILED');
    }
  });

  sock.on('START_TYPING', (data) => {
    sock.broadcast.emit('START_TYPING', data);
  });
  sock.on('END_TYPING', () => {
    sock.broadcast.emit('END_TYPING');
  });

  sock.on('SEND_MESSAGE', (data) => {
    sock.broadcast.emit('RECIEVE_MESSAGE', data);
  });
});
