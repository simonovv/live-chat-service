import * as actionTypes from '../actions/actionTypes';

const initialState = {
  nickname: ''
}

const set_nickname = (state, action) => ({
  ...state,
  nickname: action.nickname
})

const reducer = (state = initialState, action) => {
  switch(action.type) {
    case actionTypes.SET_NICKNAME: return set_nickname(state, action);
    default: return state;
  }
}

export default reducer;