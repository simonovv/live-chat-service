import * as actionTypes from '../actions/actionTypes';

const initialState = {
  messages: []
}

const addMessage = (state, action) => {
  const message = {
    author: action.author,
    message: action.message,
    time: action.time
  }
  const newState = {
    ...state,
    messages: [...state.messages, message],
  }
  return newState;
}

export default (state = initialState, action) => {
  switch(action.type) {
    case actionTypes.ADD_MESSAGE: return addMessage(state, action);
    default: return state;
  }
}