import * as actionTypes from './actionTypes';

export const set_nickname = (nickname, history) => {
  localStorage.setItem('nickname', nickname);
  history.push('/');
  return {
    type: actionTypes.SET_NICKNAME,
    nickname
  }
}