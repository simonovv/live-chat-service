import * as actionTypes from './actionTypes';

export const addMessage = (message) => ({
  type: actionTypes.ADD_MESSAGE,
  author: message.author,
  message: message.message,
  time: message.time,
});