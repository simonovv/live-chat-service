import axios from 'axios';

const auth = axios.create({
  baseURL: 'https://simonov-auth.herokuapp.com/api'
})

export default auth;