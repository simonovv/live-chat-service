import React, { Component } from 'react'

import './withErrorHandler.css';

const withErrorHandler = (WrappedComponent, axios) => {
  return class extends Component {
    state = {
      error: ''
    }

    componentWillMount() {
      this.reqInterceptor = axios.interceptors.request.use(req => {
        this.setState({ error: null });
        return req;
      });
      this.resInterceptor = axios.interceptors.response.use(res => res, error => {
        this.setState({ error: error })
        // setTimeout(() => {
        //   this.errorConfirmHandler();
        // }, 10000)
      })
    }

    componentWillUnmount() {
      axios.interceptors.request.eject(this.reqInterceptor);
      axios.interceptors.response.eject(this.resInterceptor);
    }

    errorConfirmHandler = () => {
      this.setState({ error: null })
    }

    render() {
      const error = (
        <div className='error'>
          {this.state.error && <p>{this.state.error.response.data.error}</p>}
        </div>
      );

      return(
        <React.Fragment>
          {this.state.error && error}
          <WrappedComponent {...this.props}/>
        </React.Fragment>
      );
    }
  }
}

export default withErrorHandler;