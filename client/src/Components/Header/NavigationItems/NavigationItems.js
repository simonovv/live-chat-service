import React from 'react';
import NavigationItem from './NavigationItem/NavigationItem';

import './NavigationItems.css';

const navigationItems = (props) => {
  return(
    <ul className='list'>
      {props.list.map(el => {
        return <NavigationItem 
                  title={el.title} 
                  icon={el.icon} 
                  link={el.link} 
                  key={el.id}
                />
      })}
    </ul>
  )
};

export default navigationItems;