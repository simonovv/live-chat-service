import React from 'react';
import { NavLink } from 'react-router-dom';

import './NavigaionItem.css'

const navigationItem = (props) => {
  return (
    <li className='navigation-item'>
      <NavLink
        exact={props.exact}
        to={props.link}>
        <img className='icon' src={props.icon} alt='icon'/>
      </NavLink>
    </li>
  );
}
    
export default navigationItem;