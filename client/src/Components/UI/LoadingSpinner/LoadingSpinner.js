import React from 'react';

import './LoadingSpinner.css';

const spinner = () => {
  return (
    <div className="lds-ring"><div></div><div></div><div></div><div></div></div>
  )
}

export default spinner;