import React from 'react';
import { Redirect } from 'react-router-dom';

const logout = () => {
  return (
    <React.Fragment>
      {localStorage.removeItem('nickname')}
      <Redirect to='/'/>
    </React.Fragment>
  )
}

export default logout;