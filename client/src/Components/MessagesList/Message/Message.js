import React from 'react';
import './Message.css';

const message = (props) => {
  const { from, text, time } = props;
  return (
    <div className='message'>
      <div className='wrapper'>
      <p className='from'>{from}</p>
      <p className='text'>{text}</p>
      </div>
      <p className='time'>{time}</p>
    </div>
  );
}

export default message;
