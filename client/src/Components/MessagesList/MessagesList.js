import React from 'react';
import Message from './Message/Message';

import './MessagesList.css'

const messageList = (props) => {
  const { messages } = props;
  return (
    <div className='messages-list'>
      {messages.map((message) => <Message from={message.author} text={message.message} time={message.time} key={Math.floor(Math.random() * 9999999)}/>)}
      {props.typing && <p className='typing'><span className='typing-nickname'>{props.author}</span> is typing...</p>}
    </div>
  );
}

export default messageList;
