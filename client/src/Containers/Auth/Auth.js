import React, { Component } from 'react';
import { connect } from 'react-redux';
import { auth } from '../../store/actions/auth';
import { reg } from '../../store/actions/reg';
import LoadingSpinner from '../../Components/UI/LoadingSpinner/LoadingSpinner';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import authAxios from '../../axios/axios-auth';

import './Auth.css';

class Auth extends Component {
  state = {
    login: true,
    data: {
      email: '',
      password: ''
    }
  }

  switchFormHandler = () => {
    this.setState({ login: !this.state.login, data: { email: '', password: '' } })
  }

  typeHandler = (event, element) => {
    const newData = {
      ...this.state.data,
      [element]: event.target.value,
    }

    this.setState({ data: newData })
  }

  authHandler = () => {
    this.props.auth(this.state.data.email, this.state.data.password, this.props.history);
  }

  regHandler = () => {
    this.props.reg(this.state.data.email, this.state.data.password, this.switchFormHandler);
  }

  render() {
    const loginForm = (
      <div className='form'>
        <p className='title'>SignIn</p>
        <input
          type='email'
          onChange={(event) => this.typeHandler(event, 'email')}
          placeholder='e-mail'
          value={this.state.data.email} />
        <input
          type='password'
          onChange={(event) => this.typeHandler(event, 'password')}
          placeholder='password'
          value={this.state.data.password} />
        <div className='controls-wrapper'>
          <button 
            className='control' 
            onClick={this.authHandler}>Login</button>
          <button
            className='control'
            onClick={this.switchFormHandler}>SignUp</button>
        </div>
      </div>
    );

    const signUpForm = (
      <div className='form'>
        <p className='title'>SignUp</p>
        <input
          type='email'
          onChange={(event) => this.typeHandler(event, 'email')}
          placeholder='e-mail'
          value={this.state.data.email} />
        <input
          type='password'
          onChange={(event) => this.typeHandler(event, 'password')}
          placeholder='password'
          value={this.state.data.password} />
        <div className='controls-wrapper'>
          <button 
            className='control'
            onClick={this.regHandler}>SignUp</button>
          <button
            className='control'
            onClick={this.switchFormHandler}>Back</button>
        </div>
      </div>);

      const userInfo = (
        <div>UserINFO</div>
      );

    return (
      <React.Fragment>
        {(this.props.authLoading || this.props.regLoading) && <LoadingSpinner />}
        {this.props.isAuth ? userInfo : this.state.login ? loginForm : signUpForm}
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAuth: state.auth.isAuth,
    authLoading: state.auth.loading,
    regLoading: state.reg.loading,
    reg: state.reg.reg
  }
}

const mapDispatchToProps = dispatch => {
  return {
    auth: (email, password, history) => dispatch(auth(email, password, history)),
    reg: (email, password, handler) => dispatch(reg(email, password, handler))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(Auth, authAxios));