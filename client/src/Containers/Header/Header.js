import React, { Component } from 'react';
import NavigationItems from '../../Components/Header/NavigationItems/NavigationItems';
import homeIcon from '../../assets/home.svg';
import userIcon from '../../assets/avatar.svg';
import exitIcon from '../../assets/logout.svg';

class Header extends Component {
  elements = {
    user: [
      {id: 1, title: 'Home', link: '/', icon: homeIcon},
      {id: 2, title: 'User', link: '/user', icon: userIcon},
      {id: 3, title: 'Logout', link: '/logout', icon: exitIcon}
    ]
  }

  render () {
    return (
      <NavigationItems list={this.elements.user}/>
    );
  }
};

export default Header;