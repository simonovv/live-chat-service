import React, { Component } from 'react';
import Chat from '../Chat/Chat';
import Header from '../Header/Header';
import io from 'socket.io-client';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { addMessage } from '../../store/actions/messages';
import { Route } from 'react-router-dom';
import { Switch } from 'react-router-dom';
import Logout from '../../Components/Logout/Logout';

import Welcome from '../Welcome-scene/welcome-scene';

import './Layout.css';

class Layout extends Component {
  state = {
    socket: null,
    error: false,
    typing: false,
    author: null,
    nickname: null
  }

  componentDidMount() {
    this.setState({nickname: localStorage.getItem('nickname')})
    let socket = io.connect('localhost:8090');
    this.setState({socket: socket}, () => {
      this.state.socket.on('START_TYPING', (data) => {
        clearInterval(end);
        this.setState({typing: true, author: data})
        let end = setTimeout(() => {
          this.state.socket.on('END_TYPING', () => {
          this.setState({typing: false, author: null})
          })
        }, 2000)
      })

      this.state.socket.on('RECIEVE_MESSAGE', message => {
        this.props.addMessage(message);
        console.log('RECIEVE MESSAGE ' + message);
      })
    });
  }

  setNickName = (nickname) => {
    this.state.socket.emit('SET_NICKNAME', nickname);
    this.state.socket.on('SET_NICKNAME_SUCCESS', () => {
      localStorage.setItem('nickname', nickname);
      this.setState({nickname: nickname})
    })
    this.state.socket.on('SET_NICKNAME_FAILED', () => {
      this.setState({error: true});
    })
  };

  render() {
    const chat = (
      <React.Fragment>
        <Header />
        <Chat socket={this.state.socket} startTyping={this.startTyping} author={this.state.author} endTyping={this.endTyping} typing={this.state.typing}/>
      </React.Fragment>
    )
    const { error } = this.state;
    return (
      <div className='layout'>
        <Switch>
          <Route path='/logout' component={Logout}/>
          {!this.state.nickname
            ? <Route path='/' render={() => <Welcome error={error} setNickName={this.setNickName} /> } />
            : <Route path='/' render={() => chat} />
          }
        </Switch>
      </div>
    );
  }
};

const mapDispatchToProps = dispatch => ({
  addMessage: (message) => dispatch(addMessage(message)),
});

export default connect(null, mapDispatchToProps)(withRouter(Layout));
