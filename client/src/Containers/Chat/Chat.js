import React, { Component } from 'react';
import MessagesList from '../../Components/MessagesList/MessagesList';
import { connect } from 'react-redux';
import { addMessage } from '../../store/actions/messages';
import sendIcon from '../../assets/send.svg';

import './Chat.css';

class Chat extends Component {
  state = {
    message: ''
  }

  sendMessage = (event) => {
    event.preventDefault();
    this.addMessageHandler();
    this.setState({message: ''});
  }

  typeMessageHandler = (event) => {
    this.setState({
      ...this.state,
      message: event.target.value
    })
  }
  
  startTyping = () => {
    const { socket } = this.props;
    if(socket) {
      clearTimeout(this.typing);
      socket.emit('START_TYPING', localStorage.getItem('nickname'));
      this.typing = setTimeout(this.endTyping, 2000);
    }
  }

  endTyping = () => {
    if(this.props.socket) {
      this.props.socket.emit('END_TYPING');
    }
  }

  handleKeyPress = (target) => {
    if(target.charCode === 13) {
      this.sendMessage();
    }
  }

  sendMessage = () => {
    const { message } = this.state;
    if(message) {
      const msg = {
        author: localStorage.getItem('nickname'),
        message: message,
        time: new Date().toDateString(),
      };
      this.props.socket.emit('SEND_MESSAGE', msg);
      this.props.addMessage(msg);
      this.setState({message: ''});
    }
  }

  render () {
    const { message } = this.state;
    const { messages, author, typing } = this.props;
    return (
      <div className='chat-wrapper'>
        <div className='chat'>
          <p>RoomName</p>
          <MessagesList messages={messages || null} author={author} typing={typing}/>
          <div className='controls'>
            <input
              value={message}
              onKeyPress={this.handleKeyPress}
              onChange={this.typeMessageHandler}
              onKeyDown={this.startTyping}
              type='text'
              autoFocus
            />
            <button
              onClick={this.sendMessage}>
              <img
                src={sendIcon}
                alt='send'
              />
            </button>
          </div>
        </div>
      </div>
    );
  }
};

const mapStateToProps = state => ({
  messages: state.messages.messages,
});

const mapDispatchToProps = dispatch => ({
  addMessage: (message) => dispatch(addMessage(message)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
