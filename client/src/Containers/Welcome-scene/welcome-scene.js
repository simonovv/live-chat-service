import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import './welcome-scene.css';

class Welcome extends Component {
  state = {
    nickname: ''
  }

  typeHandler = (event) => {
    const {target: { name, value }} = event;
    this.setState((prev) => ({
      ...prev,
      [name]: value,
    }));
  }

  render() {
    const { error, setNickName } = this.props;
    const { nickname } = this.state;
    return(
      <div className='welcome-scene'>
        <p className='title'>Welcome To Live Chat</p>
        <TextField 
          id='standart-bare' 
          name='nickname'
          placeholder='Your nickname'
          autoFocus={true}
          label={error && 'This nickname already exists'}
          error={error} 
          onChange={this.typeHandler}
          required
          type='text' 
          className='input'/>
        <Button 
          style={{marginTop: '20px', width: '100px'}}
          color='primary'
          variant='contained'
          onClick={() => {setNickName(nickname)}}
        >Let's GO</Button>
      </div>
    )
  }
}

export default withRouter(connect(null, null)(Welcome));