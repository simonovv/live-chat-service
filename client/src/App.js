import React, { Component } from 'react';
import Layout from './Containers/Layout/Layout';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faHome, faUser, faCommentAlt } from '@fortawesome/free-solid-svg-icons';
import './App.css';

library.add(faHome, faUser, faCommentAlt);

class App extends Component {
  render() {
    return (
      <div className='app'>
        <Layout />
      </div>
    );
  }
}

export default App;
